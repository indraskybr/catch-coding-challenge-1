## Readme

This very simple project is written in PHP (Laravel). I repeat, this is a very simple project, I did not include any complex validation and what not as the duration for
this project is very short.

To begin, first install laravel & composer. Then in command line type `composer install`, this will install all the dependencies for this project.

Connect to a clean database and run `php artisan migrate` to create all the necessary tables.

To run the project type `php artisan serve` and run it in your browser.

Set up your `.env` file and include this in your `.env` (or you can use your own).

(This is my own ,free tiered, sendgrid credentials, please use it accordingly and wisely)

````
MAIL_DRIVER=smtp
MAIL_HOST=smtp.sendgrid.net
MAIL_PORT=587
MAIL_USERNAME=apikey
MAIL_PASSWORD=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
MAIL_ENCRYPTION=tls
MAIL_FROM_NAME="Albert Tanaga"
MAIL_FROM_ADDRESS=no-reply@testmail.com
````

## Documentation 

What this app does is it will download a json line file from AWS s3 and convert it into a csv file that can be downloaded or send to your email.
The landing page will show you 2 input form, `Email` and `Filename`.
Fill in your email and your desired filename, or leave it empty. If you fill your email, it will send the csv file to your email. If you leave it empty,
the csv file will be downloaded directly. The default filename is `out.csv`, however feel free to change it by filling the filename form.
I did not use any 3rd party library, as I feel it is unnecessary for this project.

## REST API

There is a simple rest api in this project.

`GET` `{{URL}}/api/get-order-detail`

The request will return the status of the api (`true` or `false`), the message if any (if relevant) and the data fetched.
The data will be in array if the data exists in the database, and will return `null` if there is no data.

Sample Response : 
````
{
    "status": true,
    "message": "Successful!",
    "data": [
        {
            "order_id": 1001,
            "order_datetime": "2019-03-08T12:13:29+00:00",
            "total_order_value": 359.78,
            "average_unit_price": 59.96,
            "distinct_unit_count": 2,
            "total_units_count": 6,
            "customer_state": "Victoria"
        },
        {
            "order_id": 1002,
            "order_datetime": "2019-03-08T13:45:01+00:00",
            "total_order_value": 102.93,
            "average_unit_price": 14.7,
            "distinct_unit_count": 3,
            "total_units_count": 7,
            "customer_state": "New South Wales"
        },
        {
            "order_id": 1003,
            "order_datetime": "2019-03-08T14:57:31+00:00",
            "total_order_value": 680.61,
            "average_unit_price": 32.41,
            "distinct_unit_count": 4,
            "total_units_count": 21,
            "customer_state": "Victoria"
        },
		.....
		.....
		.....
	]
}
````
OR
````
{
    "status": true,
    "message": "No Data!",
    "data": null
}
````

## Contact
If you have any question, feel free to drop a message. Cheers!

ALBERT TANAGA
albert.tanaga@gmail.com