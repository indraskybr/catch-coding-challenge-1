<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\OrderDetail;

use Mail, DB;

class OrderController extends Controller
{
    public function getOrderDetail()
    {
        $order = OrderDetail::all();

        if (!count($order)) {
            $message = 'No Data!';
            $data = null;    
        } else {
            $message = 'Successful!';
            $data = $order;
        }

        $return = [
            'status' => true,
            'message' => $message,
            'data' => $data
        ];

        return response()->json($return, 200);
    }

    public function downloadData()
    {
        $file_name = 'challenge-1-in.jsonl';
        $save_dir = './';
        $complete_save_dir = $save_dir.$file_name;
        $url = 'https://s3-ap-southeast-2.amazonaws.com/catch-code-challenge/challenge-1-in.jsonl';

        if (!file_exists($complete_save_dir)) {
            //download file to local machine first
            $ch = curl_init($url);
            $fp = fopen($complete_save_dir, 'wb');
            curl_setopt($ch, CURLOPT_FILE, $fp);
            curl_setopt($ch, CURLOPT_HEADER, 0);
            curl_exec($ch);
            curl_close($ch);
            fclose($fp);
        }
    }

    public function processData($file_name, $email)
    {
        $file_name = $file_name.".csv";
        $headers = array(
            "Content-type" => "text/csv",
            "Content-Disposition" => "attachment; filename=".$file_name,
            "Pragma" => "no-cache",
            "Cache-Control" => "must-revalidate, post-check=0, pre-check=0",
            "Expires" => "0"
        );

        // set the columns for csv file
        $columns = array('order_id', 'order_datetime', 'total_order_value', 'average_unit_price', 'distinct_unit_count', 'total_units_count', 'customer_state');

        $callback = function() use ($columns, $email) {

            if ($email == null) {
                $file = fopen('php://output', 'w+');
            } else {
                $file = fopen('php://temp', 'w+');
            }
            
            fputcsv($file, $columns);

            $temp = fopen(public_path().'/challenge-1-in.jsonl','r');

            while (!feof($temp)){
                $line = fgets($temp);
                $obj = json_decode($line, true);

                // set default values
                $total_order_value = 0;
                $total_units_count = 0;
                $distinct_unit_count = 0;

                if (is_array ($obj['items'])) {
                    // loop through each items
                    foreach ($obj['items'] as $key) {
                        $total_order_value += ($key['quantity'] * $key['unit_price']);
                        $total_units_count += $key['quantity'];
                        $distinct_unit_count++;
                    }
                }

                if ($total_units_count > 0) {
                    $average_unit_price = $total_order_value / $total_units_count;
                
                    if (is_array($obj['discounts']) && count($obj['discounts'])) {
                        usort($obj['discounts'], function($a, $b) {
                            return $a['priority'] > $b['priority'];
                        });

                        foreach ($obj['discounts'] as $key) {
                            // if type = dollar, subtract the values to the total
                            // if type = percentage, calculate the value of the percentage then subtract to the total
                            if ($key['type'] == 'DOLLAR') {
                                $total_order_value -= $key['value'];
                            } else {
                                $total_order_value -= ($key['value'] / 100 * $total_order_value);
                            }
                        }
                    }

                    // set data
                    $order_id = $obj['order_id'];
                    $date_time = date('c', strtotime($obj['order_date']));
                    $total_order_value = round($total_order_value, 2);
                    $average_unit_price = round($average_unit_price, 2);
                    $customer_state = strtolower($obj['customer']['shipping_address']['state']);
                    $customer_state = ucwords($customer_state);

                    // set data to fill csv
                    fputcsv($file, array($order_id, $date_time, $total_order_value, $average_unit_price, $distinct_unit_count, $total_units_count, $customer_state));

                    $insert[] = [
                        'order_id' => $order_id,
                        'order_datetime' => $date_time,
                        'total_order_value' => $total_order_value,
                        'average_unit_price' => $average_unit_price,
                        'distinct_unit_count' => $distinct_unit_count,
                        'total_units_count' => $total_units_count,
                        'customer_state' => $customer_state,
                    ];
                }
            }

            if ($email) {
                rewind($file);
                Mail::send('email.order-detail', [], function($message) use($email, $file, $file_name) {
                    $message->to($email)->subject('Confidential Data');
                    $message->attachData(stream_get_contents($file), $filename);
                });
            }

            try {
                DB::beginTransaction();
                OrderDetail::insert($insert);
                DB::commit();
            } catch (\Exception $e) {
                DB::rollBack();
            }

            fclose($file);
        };

        $data = [
            'callback' => $callback,
            'headers' => $headers
        ];

        return $data;
    }
}
