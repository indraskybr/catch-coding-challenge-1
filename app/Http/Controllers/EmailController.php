<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;
use App\Http\Controllers\OrderController;

use Response, DateTime, Mail;

class EmailController extends Controller
{
    public function __construct()
    {
        $this->order_controller = new OrderController;
    }

    public function sendToEmail(Request $request) 
    {
        $email = $this->isNullOrEmptyString($request->email) ? null : $request->email;
        $file_name = $this->isNullOrEmptyString($request->file_name) ? "out" : $request->file_name;

        $this->order_controller->downloadData();
        $prep_data = $this->order_controller->processData($file_name, $email);

        if ($email) {
            return 'Email Sent!';
        } else {
            return Response::stream($prep_data['callback'], 200, $prep_data['headers']);
        }
    }
}
