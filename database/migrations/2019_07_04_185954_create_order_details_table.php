<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrderDetailsTable extends Migration
{
    public function up()
    {
        Schema::create('order_details', function (Blueprint $table) {
            $table->unsignedInteger('order_id');
            $table->string('order_datetime', 30);
            $table->float('total_order_value');
            $table->float('average_unit_price');
            $table->unsignedInteger('distinct_unit_count');
            $table->unsignedInteger('total_units_count');
            $table->string('customer_state', 200);
        });
    }

    public function down()
    {
        Schema::dropIfExists('order_details');
    }
}
